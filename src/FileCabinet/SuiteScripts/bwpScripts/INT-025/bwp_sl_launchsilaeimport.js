var REDIRECTMODULE, SEARCHMODULE, TASKMODULE;

/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/redirect', 'N/search', 'N/task'], runSuitelet);

function runSuitelet(redirect, search, task) {
    REDIRECTMODULE= redirect;
    SEARCHMODULE= search;
    TASKMODULE= task;
	let returnObj = {};
	returnObj['onRequest'] = _onRequest;
	return returnObj;	
}
   
/**
 * Definition of the Suitelet script trigger point.
 *
 * @param {Object} context
 * @param {ServerRequest} context.request - Encapsulation of the incoming request
 * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
 * @Since 2015.2
 */
function _onRequest(context) {
	let method = context.request.method;
	//logRecord(context, 'context');
	
	log.debug(method);
	if (method == 'GET') {
		getFunction(context);
	} else if (method == 'POST') {
		postFunction(context);
	}
}

function getFunction(context) {
	log.debug({
        title: 'Function start',
        details: 'getFunction'
    });

    let customRecordTypeId = 0;
    let searchObj = SEARCHMODULE.create({
       type: 'customrecordtype',
       filters: [['scriptid', 'is', 'customrecord_bwp_silaeimport']],
       columns: ['internalid', 'name']
    });
    customRecordTypeId = searchObj.run().getRange({ start: 0, end: 1 })[0].getValue({ name: 'internalid' });

    // Run map/reduce launcher script
	let task = TASKMODULE.create({
		taskType: TASKMODULE.TaskType.MAP_REDUCE,
		scriptId: 'customscript_bwp_mr_silaeimportlauncher',
		deploymentId: 'customdeploy_bwp_mr_silaeimportlauncher'
	});
	let taskId = task.submit();
	
	log.debug({
        title: 'Function end',
        details: 'getFunction'
    });
    REDIRECTMODULE.redirect({ url: `https://${context.request.headers.host}/app/common/custom/custrecordentrylist.nl?rectype=${customRecordTypeId}`});
}

function postFunction(context) {

}

function logRecord(logTitle, record) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);		
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}