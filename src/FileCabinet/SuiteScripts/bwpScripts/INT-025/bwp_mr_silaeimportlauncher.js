var CACHEMODULE, ERRORMODULE, FILEMODULE, FORMATMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE, TASKMODULE;

/**
 * This script is in charge to generate a temporary Customer_nnn file in Work forlder
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */
define(['N/cache', 'N/error', 'N/file', 'N/format', 'N/record', 'N/runtime', 'N/search', 'N/task'], runMapReduce);

function runMapReduce(cache, error, file, format, record, runtime, search, task) {
	CACHEMODULE= cache;
	ERRORMODULE= error;
	FILEMODULE= file;
	FORMATMODULE= format;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	TASKMODULE= task;
	
	let returnObj = {};
	returnObj['getInputData'] = _getInputData;
	returnObj['map'] = _map;
	// returnObj['reduce'] = _reduce;
	returnObj['summarize'] = _summarize;
	return returnObj;
}

/**
 * Defines the function that is executed at the beginning of the map/reduce process and generates the input data.
 * @param {Object} inputContext
 * @param {boolean} inputContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Object} inputContext.ObjectRef - Object that references the input data
 * @typedef {Object} ObjectRef
 * @property {string|number} ObjectRef.id - Internal ID of the record instance that contains the input data
 * @property {string} ObjectRef.type - Type of the record instance that contains the input data
 * @returns {Array|Object|Search|ObjectRef|File|Query} The input data to use in the map/reduce process
 * @since 2015.2
 */
function _getInputData(inputContext) {	
	log.debug('getInputData started');

	removeObsoleteDeployments();

    let silaeFiles = [];
    let fileSearchObj = SEARCHMODULE.create({
        type: SEARCHMODULE.Type.FOLDER,
        filters: [
            ['internalid', 'anyof', retrieveFolderId(mainFolderPath())],
            'AND', 
            ['file.filetype', 'anyof', 'PLAINTEXT']
        ],
        columns: ['internalid', 'name', 'file.internalid', 'file.name', 'file.filetype']
    });
    fileSearchObj.run().each(result => {
        // logRecord('result', result);
        silaeFiles.push({
            folderId: result.getValue({ name: 'internalid' }),
            fileId: result.getValue({ name: 'internalid', join: 'file' }),
            fileName: result.getValue({ name: 'name', join: 'file' })
        });
        return true;
    });

	if (silaeFiles.length == 0) {
		log.debug({
			title: 'No files to process'
		});
	}

    // Acces the cache to run the loader
    let silaeCache = CACHEMODULE.getCache({ name: 'SILAELAUNCHER' });
    silaeCache.get({
        key: 'DEPLOYMENT',
        loader: retrieveDeploymentId
    });

	log.debug('getInputData done');
	return silaeFiles;
}

/**
 * Defines the function that is executed when the map entry point is triggered. This entry point is triggered automatically
 * when the associated getInputData stage is complete. This function is applied to each key-value pair in the provided
 * context.
 * @param {Object} mapContext - Data collection containing the key-value pairs to process in the map stage. This parameter
 *     is provided automatically based on the results of the getInputData stage.
 * @param {Iterator} mapContext.errors - Serialized errors that were thrown during previous attempts to execute the map
 *     function on the current key-value pair
 * @param {number} mapContext.executionNo - Number of times the map function has been executed on the current key-value
 *     pair
 * @param {boolean} mapContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} mapContext.key - Key to be processed during the map stage
 * @param {string} mapContext.value - Value to be processed during the map stage
 * @since 2015.2
 */
function _map(context) {
	// log.debug('map started');
    let contextValue = JSON.parse(context.value)
	// logRecord('context value', contextValue);

    // Create silae record
    let silaeRecord = RECORDMODULE.create({
        type: 'customrecord_bwp_silaeimport',
        isDynamic: false
    });
    silaeRecord.setValue({
        fieldId: 'custrecord_bwp_silaeimportfilename',
        value: contextValue.fileName
    });
    silaeRecord.setValue({
        fieldId: 'custrecord_bwp_silaeimportstep',
        value: 1
    });
    let silaeRecordId = silaeRecord.save();

    // Rename the input file and move it to the workfolder
    let inputFileId = FILEMODULE.load({ id: contextValue.fileId });
    inputFileId.name = `${silaeRecordId}_${inputFileId.name}`;
    inputFileId.folder = retrieveFolderId(workFolderPath());
    // logVar('new file id', inputFileId.save());
	inputFileId.save();
	
    let silaeCache = CACHEMODULE.getCache({ name: 'SILAELAUNCHER' });

	// Create a new deployment for this particular silae file
	let deploymentRecord = RECORDMODULE.copy({
		type: RECORDMODULE.Type.SCRIPT_DEPLOYMENT,
		id: silaeCache.get({
			key: 'DEPLOYMENT',
			loader: retrieveDeploymentId
		}),
		isDynamic: true,
   	});
	// NetSuite automatically adds 'customdeploy' at the beginning of the scriptid
	deploymentRecord.setValue({
		fieldId:'scriptid',
		value: `_bwp_mr_silaeimport_${silaeRecordId}`
	});
	deploymentRecord.setValue({
		fieldId:'startdate',
		value: new Date()
	});
	deploymentRecord.setValue({
		fieldId:'title',
		value: `Silae Import ${silaeRecordId}`
	});
	let newDeploymentId = deploymentRecord.save();

	// Run the deployment to process the file
	let task = TASKMODULE.create({
		taskType: TASKMODULE.TaskType.MAP_REDUCE,
		scriptId: 'customscript_bwp_mr_silaeimport',
		deploymentId: `customdeploy_bwp_mr_silaeimport_${silaeRecordId}`,
		params: {custscript_bwp_mr_silaeimportprocessid: silaeRecordId}
	});
	let taskId = task.submit();

	//log.debug('map done');
}

/**
 * Defines the function that is executed when the reduce entry point is triggered. This entry point is triggered
 * automatically when the associated map stage is complete. This function is applied to each group in the provided context.
 * @param {Object} reduceContext - Data collection containing the groups to process in the reduce stage. This parameter is
 *     provided automatically based on the results of the map stage.
 * @param {Iterator} reduceContext.errors - Serialized errors that were thrown during previous attempts to execute the
 *     reduce function on the current group
 * @param {number} reduceContext.execut ionNo - Number of times the reduce function has been executed on the current group
 * @param {boolean} reduceContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} reduceContext.key - Key to be processed during the reduce stage
 * @param {List<String>} reduceContext.values - All values associated with a unique key that was passed to the reduce stage
 *     for processing
 * @since 2015.2
 */
function _reduce(context) {
	// log.debug('reduce started');

	//log.debug('reduce done');
}

/**
 * Defines the function that is executed when the summarize entry point is triggered. This entry point is triggered
 * automatically when the associated reduce stage is complete. This function is applied to the entire result set.
 * @param {Object} summaryContext - Statistics about the execution of a map/reduce script
 * @param {number} summaryContext.concurrency - Maximum concurrency number when executing parallel tasks for the map/reduce
 *     script
 * @param {Date} summaryContext.dateCreated - The date and time when the map/reduce script began running
 * @param {boolean} summaryContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Iterator} summaryContext.output - Serialized keys and values that were saved as output during the reduce stage
 * @param {number} summaryContext.seconds - Total seconds elapsed when running the map/reduce script
 * @param {number} summaryContext.usage - Total number of governance usage units consumed when running the map/reduce
 *     script
 * @param {number} summaryContext.yields - Total number of yields when running the map/reduce script
 * @param {Object} summaryContext.inputSummary - Statistics about the input stage
 * @param {Object} summaryContext.mapSummary - Statistics about the map stage
 * @param {Object} summaryContext.reduceSummary - Statistics about the reduce stage
 * @since 2015.2
 */
function _summarize(summary) {
	log.debug('summary started');
//	log.debug('summary', summary);

	logVar('Error count', writeErrorsToFlatFile(summary));
	
	log.debug('summary done');
}

function appendLineToErrorFile(fileObj, message) {
	if (!fileObj) {
		const fileName = `SilaeLauncher_Errors`;
		const folder = errorFolderPath();
		const folderId = retrieveFolderId(folder);
		fileObj = FILEMODULE.create({
			name: fileName,
			fileType: FILEMODULE.Type.PLAINTEXT,
			description: 'Silae launcher Errors',
			encoding: FILEMODULE.Encoding.UTF8,
			folder: folderId
		});
	}
	fileObj.appendLine({
		value: message
	});
	return fileObj;
}

function writeErrorsToFlatFile(summary) {
	let errorCount = 0;
	let fileObj;
	summary.mapSummary.errors.iterator().each(
		function(key, error, executionNo) {
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
			fileObj = appendLineToErrorFile(fileObj, message);
			errorCount++;
			return true;
		}
	);
	summary.reduceSummary.errors.iterator().each(
		function(key, error, executionNo) {
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
			fileObj = appendLineToErrorFile(fileObj, message);
			errorCount++;
			return true;
		}
	);
	if (fileObj) {
		fileObj.save();
		log.debug({
			title: 'File created',
			details: `${errorFolderPath()}/${fileObj.name}`
		});
	}
	return errorCount;
}

function mainFolderPath() { return 'Manual Imports/Silae'; }
function workFolderPath() { return `${mainFolderPath()}/Work`; }
function errorFolderPath() { return `${mainFolderPath()}/Errors`; }
function processedFolderPath() { return `${mainFolderPath()}/Processed`; }

function removeObsoleteDeployments() {
	let deploymentIds = [];
	let scriptDeploymentSearchObj = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.SCRIPT_DEPLOYMENT,
		filters: [
			['script.scriptid', 'is', 'customscript_bwp_mr_silaeimport'],
			'AND',
			['scriptid', 'isnot', 'customdeploy_bwp_mr_silaeimport']
		],
		columns: ['internalid']
	});
	scriptDeploymentSearchObj.run().each( result => { deploymentIds.push(result.getValue({ name: 'internalid' })); return true; });

	if (deploymentIds.length == 0) { return; }

	let runningDeploymentsSet = new Set();
	let scriptInstanceSearchObj = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.SCHEDULED_SCRIPT_INSTANCE,
		filters: [
		   ['scriptdeployment.internalid', 'anyof', deploymentIds], 
		   'AND', 
		   ['status', 'anyof', 'PENDING', 'PROCESSING']
		],
		columns: ['scriptdeployment.internalid']
	});
	scriptInstanceSearchObj.run().each( result => { 
		runningDeploymentsSet.add(result.getValue({ name: 'internalid' , join: 'scriptdeployment' }));
		return true; 
	});
	let runningDeployments = [...runningDeploymentsSet];
	deploymentIds.forEach( id => {
		if (runningDeployments.includes(id)) { return; }
		RECORDMODULE['delete']({
			type: RECORDMODULE.Type.SCRIPT_DEPLOYMENT,
			id: id,
		});
		log.debug({
			title: 'Deployment deleted',
			details: id
		});
	});
}

/**
 * loader function for cache.get() for SCRIPT key
 * @param {*} context 
 * @returns 
 */
function retrieveDeploymentId(context) {
	let deployementSearchObj = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.SCRIPT_DEPLOYMENT,
		filters: [
			['script.scriptid', 'is', 'customscript_bwp_mr_silaeimport'],
			'AND',
			['scriptid', 'is', 'customdeploy_bwp_mr_silaeimport']
		],
		columns: ['internalid']
	});
	return deployementSearchObj.run().getRange({ start: 0, end: 1 })[0].getValue({ name: 'internalid' });
}

function retrieveFolderId(fullPath) {
	let regex = /\//g;
	let splitResult = fullPath.split(regex);
	let parentFolderId = 0;
	let folderId = 0;
	for (let i = 0 ; i < splitResult.length ; i++) {
		parentFolderId = folderId;
		let folderName = splitResult[i];
		let nameFilter = SEARCHMODULE.createFilter({
			name: 'name',
			operator: SEARCHMODULE.Operator.IS,
			values: [folderName]
		});
		let parentFilter;
		if (parentFolderId > 0) {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.IS,
				values: [parentFolderId]
			});
		} else {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.ISEMPTY
			});				
		}
		let folderSearch = SEARCHMODULE.create({
			type: SEARCHMODULE.Type.FOLDER,
			title: 'Search Folder',
			columns: [{
				name: 'internalid'
			}, {
				name: 'parent'
			}],
			filters: [nameFilter, parentFilter]
		});
		let searchResult = folderSearch.run();
		let resultRange = searchResult.getRange({
			start: 0,
			end: 1
		});
		if (resultRange.length == 1) {
			folderId = resultRange[0].getValue({ name: 'internalid'});
		} else {
			folderId = 0;
			break;
		}
	}
	return parseInt(folderId, 10);
}

function logRecord(logTitle, record) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}