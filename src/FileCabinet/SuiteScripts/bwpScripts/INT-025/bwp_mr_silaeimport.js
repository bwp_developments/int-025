var CACHEMODULE, ERRORMODULE, FILEMODULE, FORMATMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE;

/**
 * This script is in charge to generate a temporary Customer_nnn file in Work forlder
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */
define(['N/cache', 'N/error', 'N/file', 'N/format', 'N/record', 'N/runtime', 'N/search'], runMapReduce);

function runMapReduce(cache, error, file, format, record, runtime, search) {
	CACHEMODULE= cache;  
	ERRORMODULE= error; 
	FILEMODULE= file;
	FORMATMODULE= format;
	RECORDMODULE= record; 
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	
	let returnObj = {};
	returnObj['getInputData'] = _getInputData;
	returnObj['map'] = _map;
	returnObj['reduce'] = _reduce;
	returnObj['summarize'] = _summarize;
	return returnObj;
}

/**
 * Defines the function that is executed at the beginning of the map/reduce process and generates the input data.
 * @param {Object} inputContext
 * @param {boolean} inputContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Object} inputContext.ObjectRef - Object that references the input data
 * @typedef {Object} ObjectRef
 * @property {string|number} ObjectRef.id - Internal ID of the record instance that contains the input data
 * @property {string} ObjectRef.type - Type of the record instance that contains the input data
 * @returns {Array|Object|Search|ObjectRef|File|Query} The input data to use in the map/reduce process
 * @since 2015.2
 */
function _getInputData(inputContext) {	
	log.debug('getInputData started');

	// Retrieve and validate scripts parameters
	const scriptParams = retrieveScriptParameters();
	if (!scriptParams.processId) {
		log.debug({
			title:'Process stopped',
			details: 'Process id is empty'
		});
		return;
	}
	
	log.debug({
		title:'Processing silae import',
		details: scriptParams.processId
	});

	// Retrieve and validate data from silae import record 
	let silaeImportRec = RECORDMODULE.load({
		type: 'customrecord_bwp_silaeimport',
		id: scriptParams.processId,
		isDynamic: false
	});
	// logRecord('silaeImportRec', silaeImportRec);
	const processStep = silaeImportRec.getValue({ fieldId: 'custrecord_bwp_silaeimportstep' });
	if (processStep != 1) {
		log.debug({
			title:'Process stopped',
			details: `Invalid process step ${processStep}`
		});
		return;
	}

	const inputFileName = silaeImportRec.getValue({ fieldId: 'custrecord_bwp_silaeimportfilename' });
	if (!inputFileName) {
		return { };
	}
	// Remember: to avoid duplicates, the parent process has renamed the file by adding the process id as a prefix
	const processFileName = `${scriptParams.processId}_${inputFileName}`;
	const inputFileFullPath = `${workFolderPath()}/${processFileName}`;

	// Retrieve Silae Name from first line of file
	let silaeName = '';
	let fileObj = FILEMODULE.load({ id: inputFileFullPath });
	fileObj.lines.iterator().each( line => {
		// logVar('line value', line.value);
		silaeName = line.value;
		return false;
	});
	if (!silaeName) {
		let message = `Failed to retrieve Silae Name from first line of field ${processFileName}`;
		log.debug({
			title: 'Process failed',
			details: message
		});
		
		let errorFileObj;
		errorFileObj = appendLineToErrorFile(errorFileObj, message);
		errorFileObj.save();
		log.debug({
			title: 'File created',
			details: `${errorFolderPath()}/${errorFileObj.name}`
		});
		return { };
	}
	// logVar('subsidiary', subsidiary);
	silaeImportRec.setValue({
		fieldId: 'custrecord_bwp_silaeimportsilaename',
		value: silaeName
	});
	silaeImportRec.save();

	// let silaeCache = CACHEMODULE.getCache({ name: `SILAE_${scriptParams.processId}` });
	// logVar('POLE_7', silaeCache.get({ key: 'POLE_7' }));
	// logVar('POLE_8', silaeCache.get({ key: 'POLE_8' }));
	// logVar('POLE_9', silaeCache.get({ key: 'POLE_9' }));
	// silaeCache.remove({ key: 'POLE_8' });

	// Retrieve the Subsidiary corresponding to Silae Name and cache it
	let subsidiary;
	// let subsidiarySearch = SEARCHMODULE.create({
	// 	type: SEARCHMODULE.Type.SUBSIDIARY,
	// 	filters: [['custrecord_bwp_silae_name', 'is', silaeName]],
	// 	columns: ['internalid', 'name']
	// });
	// try {
	// 	subsidiary = subsidiarySearch.run().getRange({ start: 0, end: 1 })[0].getValue({ name: 'internalid'});
	// 	// logVar('subsidiary', subsidiary);
	// }
	try {		
		let silaeCache = CACHEMODULE.getCache({ name: `SILAE_${scriptParams.processId}` });
		subsidiary = silaeCache.get({
			key: 'SUBSIDIARY',
			loader: retrieveSubsidiary(silaeName)
		});
		// logVar('subsidiary', subsidiary);
	} catch(ex) {
		// let message = `Failed to retrieve subsidiary corresponding to Silae name ${silaeName}`;
		let message = ex.message || JSON.stringify(ex);
		log.debug({
			title: 'Process failed',
			details: message
		});
		
		let errorFileObj;
		errorFileObj = appendLineToErrorFile(errorFileObj, message);
		errorFileObj.save();
		log.debug({
			title: 'File created',
			details: `${errorFolderPath()}/${errorFileObj.name}`
		});
		return { };
	}

    let inputData = {
        type: 'file',
        path: inputFileFullPath
    };
	
	log.debug('getInputData done');
	return inputData;
}

/**
 * Defines the function that is executed when the map entry point is triggered. This entry point is triggered automatically
 * when the associated getInputData stage is complete. This function is applied to each key-value pair in the provided
 * context.
 * @param {Object} mapContext - Data collection containing the key-value pairs to process in the map stage. This parameter
 *     is provided automatically based on the results of the getInputData stage.
 * @param {Iterator} mapContext.errors - Serialized errors that were thrown during previous attempts to execute the map
 *     function on the current key-value pair
 * @param {number} mapContext.executionNo - Number of times the map function has been executed on the current key-value
 *     pair
 * @param {boolean} mapContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} mapContext.key - Key to be processed during the map stage
 * @param {string} mapContext.value - Value to be processed during the map stage
 * @since 2015.2
 */
function _map(context) {
	// log.debug('map started');
	// Bypass first line
	if (context.key == '0') {
		// First line contains the SilaeName field ; it is processed at getInputData stage
		return;
	}
	// logVar('context.key', context.key);

	// Retrieve info from scripts parameters and cache
	const scriptParams = retrieveScriptParameters();

	let contextValues = parseMapContextValue(context.value);
	let dataMap = processMapValues(contextValues, scriptParams);
	// logRecord('dataMap', dataMap);

	// if (dataMap.fields.length > 0){
	if (!dataMap.metadata.bypass) {
		let value;
		if (dataMap.metadata.error) {
			value = 'ERROR';
		} else {
			value = dataMap.transactionFields;
		}
		context.write({ 
			key: dataMap.fields.trandate.sourceValue,
			value: value
		});
	}
	// if (Object.keys(dataMap.fields).length > 0) {
	// 	context.write({ 
	// 		key: dataMap.fields.trandate.sourceValue,
	// 		value: dataMap.transactionFields
	// 	});
	// }
	
	//log.debug('map done');
}

/**
 * Defines the function that is executed when the reduce entry point is triggered. This entry point is triggered
 * automatically when the associated map stage is complete. This function is applied to each group in the provided context.
 * @param {Object} reduceContext - Data collection containing the groups to process in the reduce stage. This parameter is
 *     provided automatically based on the results of the map stage.
 * @param {Iterator} reduceContext.errors - Serialized errors that were thrown during previous attempts to execute the
 *     reduce function on the current group
 * @param {number} reduceContext.execut ionNo - Number of times the reduce function has been executed on the current group
 * @param {boolean} reduceContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} reduceContext.key - Key to be processed during the reduce stage
 * @param {List<String>} reduceContext.values - All values associated with a unique key that was passed to the reduce stage
 *     for processing
 * @since 2015.2
 */
function _reduce(context) {
	// log.debug('reduce started');

	let journalEntryRecord = RECORDMODULE.create({
		type: RECORDMODULE.Type.JOURNAL_ENTRY,
		isDynamic: true
	});

	let externalid = '';
	let bodyFields;
	for (let i = 0 ; i < context.values.length ; i++) {
		if (context.values[i] == 'ERROR') {
			log.debug({
				title: 'Reduce stopped',
				details: `Map stage has encountered errors on key ${context.key}`
			});
			return;
		}

		// Set header fields values
		if (!bodyFields) {
			bodyFields = JSON.parse(context.values[0]).body;
			for (let prop in bodyFields) {
				let fieldValue = netSuiteValue(bodyFields[prop]);
				if (fieldValue) {
					journalEntryRecord.setValue({
						fieldId: prop,
						value: fieldValue
					});
					if (prop == 'externalid') {
						externalid = fieldValue;
					}
				}
			}
		}

		// Set line fields values
		let lineFields = JSON.parse(context.values[i]).line;
		// logRecord('lineFields', lineFields);
		journalEntryRecord.selectNewLine({ sublistId: 'line' });
		for (let prop in lineFields) {
			let fieldValue = netSuiteValue(lineFields[prop]);
			journalEntryRecord.setCurrentSublistValue({
				sublistId: 'line',
				fieldId: prop,
				value: fieldValue || ''
			});
		}
		journalEntryRecord.commitLine({ sublistId: 'line' });
	}

	let internalId;
	try {
		internalId = journalEntryRecord.save();
	} catch(ex) {
		log.debug({
			title: 'Transaction error',
			details: `Failed to save JO with externalid ${externalid}`
		});
		throw ex;
	}
	let fieldsValues = SEARCHMODULE.lookupFields({
		type: SEARCHMODULE.Type.JOURNAL_ENTRY,
		id: internalId,
		columns: ['tranid']
	});
	let tranid = '';
	if ('tranid' in fieldsValues) {
		tranid = fieldsValues['tranid'];
	}
	logVar('internalId', `trandate ${context.key} saved as journalentry ${tranid} (${internalId})`);
	context.write({
		key: context.key,
		value: `${tranid} (${internalId}) / date: ${context.key}`
	});
	
	//log.debug('reduce done');
}

/**
 * Defines the function that is executed when the summarize entry point is triggered. This entry point is triggered
 * automatically when the associated reduce stage is complete. This function is applied to the entire result set.
 * @param {Object} summaryContext - Statistics about the execution of a map/reduce script
 * @param {number} summaryContext.concurrency - Maximum concurrency number when executing parallel tasks for the map/reduce
 *     script
 * @param {Date} summaryContext.dateCreated - The date and time when the map/reduce script began running
 * @param {boolean} summaryContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Iterator} summaryContext.output - Serialized keys and values that were saved as output during the reduce stage
 * @param {number} summaryContext.seconds - Total seconds elapsed when running the map/reduce script
 * @param {number} summaryContext.usage - Total number of governance usage units consumed when running the map/reduce
 *     script
 * @param {number} summaryContext.yields - Total number of yields when running the map/reduce script
 * @param {Object} summaryContext.inputSummary - Statistics about the input stage
 * @param {Object} summaryContext.mapSummary - Statistics about the map stage
 * @param {Object} summaryContext.reduceSummary - Statistics about the reduce stage
 * @since 2015.2
 */
function _summarize(summary) {
	log.debug('summary started');
//	log.debug('summary', summary);

	const scriptParams = retrieveScriptParameters();
	if (!scriptParams.processId) return;
	// logVar('Error count', writeErrorsToFlatFile(summary, scriptParams.processId));
	// return;

	// Load the input file 
	let silaeImportRec = RECORDMODULE.load({
		type: 'customrecord_bwp_silaeimport',
		id: scriptParams.processId,
		isDynamic: false
	});
	// Remember: to avoid duplicates, the parent process has renamed the file by adding the process id as a prefix
	const inputFileFullPath = `${workFolderPath()}/${scriptParams.processId}_${silaeImportRec.getValue({ fieldId: 'custrecord_bwp_silaeimportfilename' })}`;
	let fileObj = FILEMODULE.load({ id: inputFileFullPath });

	let processSummaryText = '';
	let errorMessage = '';
	if (writeErrorsToFlatFile(summary, scriptParams.processId) > 0) {
		fileObj.folder = retrieveFolderId(errorFolderPath());
		errorMessage = 'Errors during data import - see error file for details';
	} else {
		fileObj.folder = retrieveFolderId(processedFolderPath());
	}
	
	summary.output.iterator().each((key, value) => {
		// logRecord('key', key);
		// logRecord('value', value);
		processSummaryText = processSummaryText || `Transactions created: \n`;
		processSummaryText += `- ${value}\n`;
		return true;
	});
	
	fileObj.save();
	silaeImportRec.setValue({
		fieldId: 'custrecord_bwp_silaeimportstep',
		value: 2
	});
	silaeImportRec.setValue({
		fieldId: 'custrecord_bwp_silaeimportsummary',
		value: processSummaryText
	});
	silaeImportRec.setValue({
		fieldId: 'custrecord_bwp_silaeimporterrormsg',
		value: errorMessage
	});
	silaeImportRec.save();
	
	log.debug('summary done');
}

function netSuiteValue(field) {
	let returnValue = field.destValue;
	if (field.destType) {
		returnValue = FORMATMODULE.parse({
			value: field.destValue,
			type: field.destType
		});
	}
	// logRecord('field', field);
	// logVar('returnValue', returnValue);
	return returnValue;
}

function processMapValues(mapValues, scriptParams) {
	let dataMap = mapData(mapValues, scriptParams);
	applyFunctions(dataMap);
	return dataMap;
}

/**
 * Generates loader function for cache.get() for keys related to Account record
 * @param {*} accountNumber (2748000 for example)
 * @returns 
 */
function searchAndCacheAccount(accountNumber) {
	return context => {
		log.debug({ title: 'Cache loader', details: `searchAndCacheAccount for ${context.key}` });
		const scriptParams = retrieveScriptParameters();
		let searchObj = SEARCHMODULE.create({
			type: SEARCHMODULE.Type.ACCOUNT,
			filters: [['number', 'is', accountNumber.toString()]],
			columns: ['internalid', 'name']
		});
		let searchResult = searchObj.run().getRange({ start: 0, end: 1 })[0];
		let cacheValues = { };
		try {
			cacheValues[`ACCOUNT_${accountNumber}`] = searchResult.getValue({ name: 'internalid' });
			cacheValues[`ACCOUNT_NAME_${accountNumber}`] = searchResult.getValue({ name: 'name' });
		} catch(ex) {			
			throw ERRORMODULE.create({
				name: 'BWP_INVALID_VALUE',
				message: `message: failed to retrieve account with account number ${accountNumber}`
			});
		}
		let silaeCache = CACHEMODULE.getCache({ name: `SILAE_${scriptParams.processId}` });
		for (prop in cacheValues) {
			if (prop == context.key) {
				continue;
			}
			silaeCache.put({
				key: prop,
				value: cacheValues[prop]
			});
		}
		return cacheValues[context.key]
	}
}

/**
 * loader function for cache.get() for keys related to Silae Import record
 * @param {*} context 
 * @returns 
 */
function loadAndCacheSilaeImport(context) {
	log.debug({ title: 'Cache loader', details: `loadAndCacheSilaeImport for ${context.key}` });
	const scriptParams = retrieveScriptParameters();
	let silaeImportRec = RECORDMODULE.load({
		type: 'customrecord_bwp_silaeimport',
		id: scriptParams.processId,
		isDynamic: false
	});
	let cacheValues = {
		SILAE_NAME: silaeImportRec.getValue({ fieldId: 'custrecord_bwp_silaeimportsilaename' }),
		// PROCESS_DATE: silaeImportRec.getValue({ fieldId: 'created' }).toISOString()
		PROCESS_DATE: FORMATMODULE.format({ 
			value: silaeImportRec.getValue({ fieldId: 'created' }),
			type: FORMATMODULE.Type.DATE
		})
	}
	let silaeCache = CACHEMODULE.getCache({ name: `SILAE_${scriptParams.processId}` });
	for (prop in cacheValues) {
		if (prop == context.key) {
			continue;
		}
		silaeCache.put({
			key: prop,
			value: cacheValues[prop]
		});
	}
	return cacheValues[context.key]
}

/**
 * Generates loader function for cache.get() for SUBSIDIARY key
 * @param {string} silaeName 
 * @returns 
 */
function retrieveSubsidiary(silaeName) {
	return context => {
		log.debug({ title: 'Cache loader', details: `retrieveSubsidiary for ${context.key}` });
		let searchObj = SEARCHMODULE.create({
			type: SEARCHMODULE.Type.SUBSIDIARY,
			filters: [['custrecord_bwp_silae_name', 'is', silaeName]],
			columns: ['internalid', 'name']
		});
		try {
			return searchObj.run().getRange({ start: 0, end: 1 })[0].getValue({ name: 'internalid' });
		} catch(ex) {
			throw ERRORMODULE.create({
				name: 'BWP_INVALID_VALUE',
				message: `message: failed to retrieve subsidiary with Silae name ${silaeName}`
			});
		}
	}
}

/**
 * Generates loader function for cache.get() for SUBSIDIARY key
 * @param {string} silaeCC 
 * @returns 
 */
function retrieveCC(silaeCC) {
	return context => {
		// log.debug({ title: 'Cache loader', details: `retrieveCC for ${context.key}` });
		let searchObj = SEARCHMODULE.create({
			type: 'customrecord_cseg_bwp_cc',
			filters: [['externalid', 'is', silaeCC]],
			columns: ['internalid', 'name']
		});
		try {
			let cc = searchObj.run().getRange({ start: 0, end: 1 })[0].getValue({ name: 'internalid' });
			log.debug({ title: 'Cache loader', details: `retrieveCC for ${context.key} --> ${cc}` });
			return cc;
		} catch(ex) {
			log.error({ title: 'Cache loader', details: `retrieveCC failed for ${context.key}` });
			throw ERRORMODULE.create({
				name: 'BWP_INVALID_VALUE',
				message: `message: failed to retrieve cost center with externalid ${silaeCC}`
			});
		}
	}
}

/**
 * Generates loader function for cache.get() for SUBSIDIARY key
 * @param {string} silaePole 
 * @returns 
 */
function retrievePole(silaePole) {
	return context => {
		// log.debug({ title: 'Cache loader', details: `retrievePole for ${context.key}` });
		let searchObj = SEARCHMODULE.create({
			type: 'customrecord_csegcseg_bwp_pole',
			filters: [['externalid', 'is', silaePole]],
			columns: ['internalid', 'name']
		});
		try {
			let silaePole = searchObj.run().getRange({ start: 0, end: 1 })[0].getValue({ name: 'internalid' });
			log.debug({ title: 'Cache loader', details: `retrievePole for ${context.key} --> ${silaePole}` });
			return silaePole;
		} catch(ex) {
			log.error({ title: 'Cache loader', details: `retrievePole failed for ${context.key}` });	
			throw ERRORMODULE.create({
				name: 'BWP_INVALID_VALUE',
				message: `message: failed to retrieve pole with externalid ${silaePole}`
			});
		}
	}
}

function appendLineToErrorFile(fileObj, message) {
	if (!fileObj) {
		const scriptParams = retrieveScriptParameters();
		const fileName = `Silae_Errors_${scriptParams.processId}`;
		const folder = errorFolderPath();
		const folderId = retrieveFolderId(folder);
		fileObj = FILEMODULE.create({
			name: fileName,
			fileType: FILEMODULE.Type.PLAINTEXT,
			description: 'Silae Import Errors',
			encoding: FILEMODULE.Encoding.UTF8,
			folder: folderId
		});
	}
	fileObj.appendLine({
		value: message
	});
	return fileObj;
}

function writeErrorsToFlatFile(summary, extractionId, error) {
	let errorCount = 0;
	let fileObj;
	summary.mapSummary.errors.iterator().each(
		function(key, error, executionNo) {
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
			fileObj = appendLineToErrorFile(fileObj, message);
			errorCount++;
			return true;
		}
	);
	summary.reduceSummary.errors.iterator().each(
		function(key, error, executionNo) {
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
			fileObj = appendLineToErrorFile(fileObj, message);
			errorCount++;
			return true;
		}
	);
	if (fileObj) {
		fileObj.save();
		log.debug({
			title: 'File created',
			details: `${errorFolderPath()}/${fileObj.name}`
		});
	}
	return errorCount;
}

function writeDataToFlatFile(summary, extractionId) {
	let fileId = 0;
	let factorRec = RECORDMODULE.load({
		type: 'customrecord_bwp_factorextraction',
		id: extractionId,
		isDynamic: false
	});
	const timestamp = factorRec.getValue({ fieldId: 'custrecord_bwp_factorextracttimestamp' });

	// Generate first and last lines based on subsidiary's information
	let subsidiaryRec = RECORDMODULE.load({
		type: RECORDMODULE.Type.SUBSIDIARY,
		id: factorRec.getValue({ fieldId: 'custrecord_bwp_factorextractsubsidiary' }),
		isDynamic: false
	});
	const codeVendeurCedant = subsidiaryRec.getValue({ fieldId: 'custrecord_bwp_code_vendeur_cedant' });

	let firstLine;
	let lastLine;
	try {
		firstLine = generateFirstLastOutputLine(subsidiaryRec, true);
		lastLine = generateFirstLastOutputLine(subsidiaryRec, false);
	} catch (ex) {
		log.debug({
			title: 'error',
			details: ex
		});
	}

	// Set temporary filename and write data
	// const fileName = `Customer_${extractionId}`;
	const fileName = `${codeVendeurCedant} - Export_Clients du ${timestamp}`;
	const folder = 'FactorFiles/Work';
	const folderId = retrieveFolderId(folder);
	let fileObj;

	summary.output.iterator().each((key, value) => {
		switch (key) {
			case 'OutputLine':
				// log.debug({
				// 	title: key,
				// 	details: value
				// });
				if (!fileObj) {
					try {
						let oldFile = FILEMODULE.load({
							id: `${folder}/${fileName}`
						});
						FILEMODULE.delete({ id: oldFile.id });
					} catch (ex) { }
					fileObj = FILEMODULE.create({
						name: fileName,
						fileType: FILEMODULE.Type.PLAINTEXT,
						description: 'Customer Factor Extract ' + extractionId,
						encoding: FILEMODULE.Encoding.UTF8,
						folder: folderId
					});
					fileObj.appendLine({
						value: firstLine
					});
				}
				fileObj.appendLine({
					value: value
				});
				break;
		}
		return true;
	});
	if (fileObj) {
		fileObj.appendLine({
			value: lastLine
		});
		fileId = fileObj.save();
		log.debug({
			title: 'File created',
			details: `${folder}/${fileName}`
		});
	}
	return fileId;
}

function retrieveScriptParameters() {
	let script = RUNTIMEMODULE.getCurrentScript();
	return {
		processId: script.getParameter({ name: 'custscript_bwp_mr_silaeimportprocessid' }) || 10
	}

	// Code to retrieve the last silae import record
	var customrecord_bwp_silaeimportSearchObj = SEARCHMODULE.create({
		type: "customrecord_bwp_silaeimport",
		filters:
		[
		],
		columns:
		[
			SEARCHMODULE.createColumn({
			  name: "internalid",
			  summary: "MAX"
		   })
		]
	 });
	 let searchResult = JSON.parse(JSON.stringify(customrecord_bwp_silaeimportSearchObj.run().getRange({ start: 0, end: 1 })[0]));
	 let internalid = searchResult.values[Object.keys(searchResult.values)[0]];
	 logVar('internalid', internalid);
}

function parseMapContextValue(contextValue) {
	// logRecord('contextValue', contextValue);
	let parsedValues = { };
	// Process a fixed length text line
	let lineFields = { 
		codJournal: 3, 
		Datecompta: 6, 
		txtFiller1: 2, 
		codCompte: 7, 
		txtFiller2: 6, 
		CodAna: 1, 
		codAnalytique1: 2, 
		codAnalytique2: 11, 
		PeriodPaye: 12, 
		txtFiller3: 33, 
		txtSens: 1, 
		TxtAmount: 20
	};
	let regexText = '^';
	let props = [];
	for (let prop in lineFields) {
		props.push(prop);
		regexText += `(.{${lineFields[prop]}})`;
	}
	let regex =  new RegExp(regexText);
	let values = regex.exec(contextValue);
	// logVar('values', values);
	for (let i = 1 ; i < values.length ; i++) {
		parsedValues[props[i-1]] = values[i];
	}
	parsedValues.dataSource = contextValue;
	// logRecord('parsedValues', parsedValues);
	return parsedValues;
}

/**
 * Map each value expected in the output with the corresponding value from the input
 * Set data processors that are in charge to validate and format the data to output
 * Data processors must be functions that return arrow function
 * @param {*} mapValues 
 * @returns {Object} outputData 
 * @returns {string} outputData.vendorCode
 * @returns {string} outputData.customerCode
 * @returns {string} outputData.customerIdentifier
 * @returns {string} outputData.customerAcronym
 * @returns {string} outputData.customerCompanyName
 * @returns {string} outputData.address
 * @returns {string} outputData.additionalAddress
 * @returns {string} outputData.postalCode
 * @returns {string} outputData.city
 * @returns {string} outputData.country
 * @returns {string} outputData.phone
 */
function mapData(mapValues, scriptParams) {
	let silaeCache = CACHEMODULE.getCache({ name: `SILAE_${scriptParams.processId}`});
	let outputData = {
		metadata: {
			sources: {
				customrecord_bwp_silaeimport: scriptParams.processId,
				textFileLine: mapValues.dataSource
			}, 
			error: false,
			bypass: false
		},
		fields: {
			subsidiary: {
				fieldSource: 'textFileLine.SilaeName',
				sourceValue: silaeCache.get({ 
					key: 'SILAE_NAME',
					loader: loadAndCacheSilaeImport
				}),
				dataProcessors: [checkMandatory(), transcoSubsidiary()]
			},
			trandate: {
				fieldSource: 'textFileLine.Datecompta',
				sourceValue: mapValues['Datecompta'],
				dataProcessors: [checkMandatory(), parseSilaeDate()],
				destType: FORMATMODULE.Type.DATE
			},
			memoBody: {
				fieldSource: 'textFileLine.Datecompta',
				sourceValue: silaeCache.get({ 
					key: 'PROCESS_DATE',
					loader: loadAndCacheSilaeImport
				}),
				dataProcessors: [formatMemoBody()]
			},
			externalid: {
				fieldSource: 'textFileLine.Datecompta',
				sourceValue: {
					silaeName: silaeCache.get({ 
						key: 'SILAE_NAME',
						loader: loadAndCacheSilaeImport
					}),
					dateCompta: mapValues['Datecompta'],
					processDate: silaeCache.get({ 
						key: 'PROCESS_DATE',
						loader: loadAndCacheSilaeImport
					})
				},
				dataProcessors: [formatExternalId()]
			},
			account: {
				fieldSource: 'textFileLine.codCompte && CodAna',
				sourceValue: {
					codCompte: mapValues['codCompte'],
					codAna: mapValues['CodAna']
				},
				dataProcessors: [checkMandatory(), transcoAccount(), validateAccount()]
			},
			credit: {
				fieldSource: 'textFileLine.TxtAmount && txtSens',
				sourceValue: {
					txtAmount: mapValues['TxtAmount'],
					txtSens: mapValues['txtSens']
				},
				dataProcessors: [amountToCredit(), checkMandatory()]
			},
			debit: {
				fieldSource: 'textFileLine.Datecompta',
				sourceValue: {
					TxtAmount: mapValues['TxtAmount'],
					txtSens: mapValues['txtSens']
				},
				dataProcessors: [amountToDedit(), checkMandatory()]
			},
			memoLine: {
				fieldSource: 'textFileLine.codCompte',
				sourceValue: mapValues['codCompte'],
				dataProcessors: [transcoAccountName()]
			},
			csegcseg_bwp_pole: {
				fieldSource: 'textFileLine.codAnalytique1',
				sourceValue: mapValues['codAnalytique1'],
				dataProcessors: [transcoPole()]
			},
			cseg_bwp_cc: {
				fieldSource: 'textFileLine.codAnalytique2',
				sourceValue: mapValues['codAnalytique2'],
				dataProcessors: [transcoCC()]
			},
			custcol_bwp_notilus_num_doc_ext: {
				fieldSource: 'textFileLine.PeriodPaye',
				sourceValue: mapValues['PeriodPaye'],
				dataProcessors: []
			},
		}
	};
	
	outputData.transactionFields = {
		body: {
			subsidiary: outputData.fields.subsidiary,
			trandate: outputData.fields.trandate,
			memo: outputData.fields.memoBody,
			externalid: outputData.fields.externalid
		},
		line: {
			account: outputData.fields.account,
			credit: outputData.fields.credit,
			debit: outputData.fields.debit,
			memo: outputData.fields.memoLine,
			csegcseg_bwp_pole: outputData.fields.csegcseg_bwp_pole,
			cseg_bwp_cc: outputData.fields.cseg_bwp_cc,
			custcol_bwp_notilus_num_doc_ext: outputData.fields.custcol_bwp_notilus_num_doc_ext
		}
	}
	// logRecord('outputData', outputData);
	return outputData;
}

function applyFunctions(dataMap) {
	// log.debug({
	// 	title: 'Function start',
	// 	details: 'applyFunctions'
	// });
	let fields = dataMap.fields;
	for (let key in fields) {
		try {
			fields[key].destValue = applyFieldFunctions(fields[key].sourceValue, [...(fields[key].dataProcessors || [])]);
		} catch (ex) {
			if (ex.name && ex.name.startsWith('BWP_') && ex.message) {
				const regex = /(^\w+)\.(.+)/;
				let regexRes = regex.exec(fields[key].fieldSource);
				let source = '';
				let field = '';
				let sourceInfo = '';
				if (regexRes) {
					source = regexRes[1];
					field = regexRes[2];
					sourceInfo = dataMap.metadata.sources[source];
				}
				let message = `${source} ${sourceInfo} - field ${field} - ${ex.message}`;
				if (ex.name == 'BWP_EXCLUDE_THIS_RESULT') {
					// This result may not be extracted ==> reset fields
					dataMap.fields = {};
					dataMap.metadata.bypass = true;
					log.debug({
						title: 'Data exclusion',
						details: message
					});
					break;
				}
				dataMap.metadata.error = true;
				throw ERRORMODULE.create({
					name: ex.name,
					message: message,
					notifyOff: true
				});
			}
			dataMap.metadata.error = true;
			throw ex;
		}
	}
}

/**
 * apply data processors functions in a recursive way
 * @param {*} stringValue 
 * @param {*} functions 
 * @returns 
 */
function applyFieldFunctions(fieldValue, functions) {
	if (functions.length == 0) {
		return fieldValue;
	}
	let func = functions.pop();
	if (!(typeof func === 'function')) {
		func = a => a;
		log.debug('not a function');
	}
	return applyFieldFunctions(func(fieldValue), functions);
}

/**
 * b is an object
 * 	b.silaeName
 * 	b.dateCompta
 * 	b.processDate
 */
function formatExternalId() {
	return b => `IMP_SILAE_${transcoSubsidiary()(b[Object.keys(b)[0]])}${b[Object.keys(b)[1]]}${stringDateToDDMMYYYY(b[Object.keys(b)[2]])}`;
}

/**
 * b is a string date formatted with format module
 * @returns 
 */
function formatMemoBody() {
	return b => `Import SILAE ${stringDateToDDMMYYYY(b)}`;
}

/**
 * 
 * @param {*} stringDate a string date coming from format.format()
 * @returns string date as DD/MM/YYYY
 */
function stringDateToDDMMYYYY(stringDate) {
	const regex = /^(\d+)-(\d+)-(\d+)/;
	let regexResult = regex.exec(FORMATMODULE.parse({ value: stringDate, type: FORMATMODULE.Type.DATE }).toISOString());
	return `${regexResult[3]}/${regexResult[2]}/${regexResult[1]}`;
}

/**
 * b is a DDMMYY date
 * @returns arrow function that format date according to NetSuite API
 */
function parseSilaeDate() {
	const regex = /(\d{2})/g;
	return b => {
		let matchRex = b.match(regex);
		try{
			return FORMATMODULE.format({ 
				value: new Date(2000 + parseInt(matchRex[2], 10), parseInt(matchRex[1], 10) - 1, parseInt(matchRex[0], 10), 0, 0, 0),
				type: FORMATMODULE.Type.DATE
			});
		} catch(ex) {
			throw ERRORMODULE.create({
				name: 'BWP_INVALID_VALUE',
				message: `message: failed to parse date ${b}`
			});
		}
	}
}

/**
 * b is an object 
 * 	b.txtAmount = amount
 * 	b.txtSens = credit or debit
 * @returns 
 */
function amountToCredit() {
	return b => b[Object.keys(b)[1]] != 'D' ? b[Object.keys(b)[0]] : 0;
}

/**
 * b is an object 
 * 	b.txtAmount = amount
 * 	b.txtSens = credit or debit
 * @returns 
 */
function amountToDedit() {
	return b => b[Object.keys(b)[1]] == 'D' ? b[Object.keys(b)[0]] : 0;
}

/**
 * b is an account number (4120000 for exemple)
 * @returns 
 */
function transcoAccount() {
	const scriptParams = retrieveScriptParameters();
	let silaeCache = CACHEMODULE.getCache({ name: `SILAE_${scriptParams.processId}` });
	return b => silaeCache.get({
			key: `ACCOUNT_${b}`,
			loader: searchAndCacheAccount(b)
	});
}

/**
 * b is an account number (4120000 for exemple)
 * @returns 
 */
function transcoAccountName() {
	const scriptParams = retrieveScriptParameters();
	let silaeCache = CACHEMODULE.getCache({ name: `SILAE_${scriptParams.processId}` });
	return b => silaeCache.get({
		key: `ACCOUNT_NAME_${b}`,
		loader: searchAndCacheAccount(b)
	});
}

function transcoSubsidiary() {
	const scriptParams = retrieveScriptParameters();
	let silaeCache = CACHEMODULE.getCache({ name: `SILAE_${scriptParams.processId}` });
	return b => silaeCache.get({
		key: 'SUBSIDIARY',
		loader: retrieveSubsidiary(b)
	});
}

function transcoCC() {
	return b => {
		if (!b.toString().trimStart()) {
			return '';
		}
		let silaeCC = b.toString().substring(0, 6);
		const scriptParams = retrieveScriptParameters();
		let silaeCache = CACHEMODULE.getCache({ name: `SILAE_${scriptParams.processId}` });
		try {
			return silaeCache.get({
				key: `CC_${silaeCC}`,
				loader: retrieveCC(silaeCC)
			});
		} catch(ex) {
			throw ERRORMODULE.create({
				name: 'BWP_INVALID_VALUE',
				message: `message: failed to retrieve cost center with externalid ${silaeCC}`
			});
		}
	};
}

function transcoPole() {
	// Silae pole is nn-xxxxx ; regex to retrieve nn part
	let regex = /^(.+)-/;
	return b => {
		if (!b.toString().trimStart()) {
			return '';
		}
		let silaePole;
		try {
			silaePole = regex.exec(b)[1];
		} catch(ex) {
			throw ERRORMODULE.create({
				name: 'BWP_INVALID_VALUE',
				message: `message: failed to extract pole value from ${b}`
			});
		}
		const scriptParams = retrieveScriptParameters();
		let silaeCache = CACHEMODULE.getCache({ name: `SILAE_${scriptParams.processId}` });
		try {
			return silaeCache.get({
				key: `POLE_${silaePole}`,
				loader: retrievePole(silaePole)
			});
		} catch(ex) {
			throw ERRORMODULE.create({
				name: 'BWP_INVALID_VALUE',
				message: `message: failed to retrieve pole with externalid ${silaePole}`
			});
		}
	};
}

/**
 * b is an object
 * 	b.codCompte
 * 	b.codAna
 * @returns accountnumber (b.codCompte) in case validation is successfull
 */
function validateAccount() {
	return b => {
		let accountNumber = b[Object.keys(b)[0]];
		let codAna = b[Object.keys(b)[1]];
		if (!(codAna == 'A' || codAna == ' ' && ['2', '4'].includes(accountNumber.toString()[0]))) {
			throw ERRORMODULE.create({
				name: 'BWP_EXCLUDE_THIS_RESULT',
				message: `message: combination of account number ${accountNumber} and codAna ${codAna} is out of scope`
			});
		}
		return accountNumber;
	};
}

function replaceChars(charsToReplace, replacementChar) {
	let regex;
	if (charsToReplace instanceof RegExp) {
		regex = charsToReplace;
	} else {
		let reservedCharsRegEx = /([\.\?\+\*])/g;
		regex = new RegExp('[' + charsToReplace.replace(reservedCharsRegEx, '\\$1') + ']', 'g');
	}
	return b => b && b.toString().length > 0 ? b.toString().replace(regex, replacementChar) : b;
}

function checkMandatory() {
	return b => {
		if (!b) {
			throw ERRORMODULE.create({
				name: 'BWP_INVALID_VALUE',
				message: 'message: empty data not allowed'
			});
		}
		return b;
	};
}

function truncate(a) {
	return b => b && b.toString().length > a ? b.toString().trimStart().substring(0, a) : b;
}

function trimSpace() {
	return b => b && b.toString().length > 0 ? b.toString().trim() : b;
}

function mainFolderPath() { return 'Manual Imports/Silae'; }
function workFolderPath() { return `${mainFolderPath()}/Work`; }
function errorFolderPath() { return `${mainFolderPath()}/Errors`; }
function processedFolderPath() { return `${mainFolderPath()}/Processed`; }

function retrieveFolderId(fullPath) {
	let regex = /\//g;
	let splitResult = fullPath.split(regex);
	let parentFolderId = 0;
	let folderId = 0;
	for (let i = 0 ; i < splitResult.length ; i++) {
		parentFolderId = folderId;
		let folderName = splitResult[i];
		let nameFilter = SEARCHMODULE.createFilter({
			name: 'name',
			operator: SEARCHMODULE.Operator.IS,
			values: [folderName]
		});
		let parentFilter;
		if (parentFolderId > 0) {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.IS,
				values: [parentFolderId]
			});
		} else {
			parentFilter = SEARCHMODULE.createFilter({
				name: 'parent',
				operator: SEARCHMODULE.Operator.ISEMPTY
			});				
		}
		let folderSearch = SEARCHMODULE.create({
			type: SEARCHMODULE.Type.FOLDER,
			title: 'Search Folder',
			columns: [{
				name: 'internalid'
			}, {
				name: 'parent'
			}],
			filters: [nameFilter, parentFilter]
		});
		let searchResult = folderSearch.run();
		let resultRange = searchResult.getRange({
			start: 0,
			end: 1
		});
		if (resultRange.length == 1) {
			folderId = resultRange[0].getValue({ name: 'internalid'});
		} else {
			folderId = 0;
			break;
		}
	}
	return parseInt(folderId, 10);
}

function logRecord(logTitle, record) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);
	}
}

function logVar(title, value) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}